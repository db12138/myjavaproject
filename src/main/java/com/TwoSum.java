package com;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

class TwoSumClass {
    public Integer twoSum(Integer a,Integer b){
        return a+b;
    }
    public ArrayList readFile(String fileName) throws Exception {
        ArrayList<ArrayList<Integer> > inputNums = new ArrayList<>();
        try (Scanner sc = new Scanner(new FileReader(fileName))) {
            while (sc.hasNextLine()) {  //按行读取字符串
                String line = sc.nextLine();
                String [] valueArr = line.split(" ");
                ArrayList<Integer> intArr = new ArrayList<>();
                for(int i=0;i< valueArr.length;i++)
                {
                    Integer cur = Integer.parseInt(valueArr[i]);
                    intArr.add(cur);
                }
                inputNums.add(intArr);
            }
        }
        return inputNums;
    }
    public boolean twoSumTest(String inputFileName,String outputFileName) throws Exception{
        ArrayList<ArrayList<Integer> > inputNums = readFile(inputFileName);
        ArrayList<ArrayList<Integer> > outputNums = readFile(outputFileName);
        System.out.println(inputNums);
        System.out.println(outputNums);
        boolean res = true;
        for(int i=0;i<inputNums.size();i++)
        {
            Integer a = inputNums.get(i).get(0);
            Integer b = inputNums.get(i).get(1);
            Integer c = outputNums.get(i).get(0);
            if(twoSum(a,b) != c){
                res = false;
                return res;
            }
        }
        return res;
    }
}

public class TwoSum {
    public static void main(String [] args) throws Exception{
        TwoSumClass x = new TwoSumClass();
        String inputFileName = args[0];
        String outputFileName = args[1];
        System.out.println(inputFileName);
        System.out.println(outputFileName);

        boolean res = x.twoSumTest(inputFileName,outputFileName);
        if(res){
            System.out.println("Test Success");
        }
        else{
            System.out.println("Test Failure");
        }
        System.out.println("HelloWorld");
    }
}
